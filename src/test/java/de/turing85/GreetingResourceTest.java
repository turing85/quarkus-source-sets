package de.turing85;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.Test;

public class GreetingResourceTest {
  @Test
  void shouldReturnExpectedValue() {
    // GIVEN: nothing

    // WHEN
    String actual = new GreetingResource().hello();

    // THEN
    assertThat(actual).isEqualTo(GreetingResource.HELLO);
  }
}