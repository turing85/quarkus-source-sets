# Using multiple source directories with quarkus

In this project, we want to use multiple soruce directories in order to separate our code into:

- production code
- unit test code
- integration test code (`@QuarkusTest`s)
- black bock test code (`@QuarkusIntegrationTest`s)

Thus, our source tree looks as follows:

```
 └──src
     ├── blackboxtest
     ├── integrationtest
     ├── main
     └── test
```

The semantics are as follows:

- if we run `./mvnw package`, we want the tests in `src/test` to execute
- if we run `./mvnw verify`, we want the tests in `src/test` and `src/integrationtest` to execute
- if we run `./mvnw -Pnative verify`, we want the tests in `src/test` and `src/blackboxtest` to execute

For this, some configuration in the [`pom.xml`](./pom.xml) is necessary to set up the different source sets and their output directories, configure compilation and dependencies, ... Discussing the whole `pom.xml` here would be lengthy. I encourage the curious reader to take a look for themselves. After compilation, we have an output directory structure of
```
└──target
     ├── blackboxtest-classes
     ├── classes
     ├── generated-blackboxtest-sources
     ├── generated-integrationtest-sources
     ├── generated-sources
     ├── generated-test-sources
     ├── integrationtest-classess
     └── test-classes
```

The configurations of surefire and failsafe take care that the right tests are executed in the right phases.